package com.knock.shapeorder

import android.graphics.Point
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.knock.shapeorder.database.MyCircle
import com.knock.shapeorder.utilities.CircleShape
import com.knock.shapeorder.viewmodels.CircleListViewModel
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.setContentView
import javax.inject.Inject

class MainActivity : AppCompatActivity(),View.OnClickListener {

    private var x = 120
    private var y = 120
    private var counter = 0
    private var width: Int = 0
    private var height: Int = 0
    private var countInRow: Int = 0
    private var countInColumn: Int = 0
    private var totalCount: Int = 0
    private val bottomLayoutHeight = 110
    private var rlMain: RelativeLayout? = null
    private var style = 'h'
    private var btnShape: Button? = null
    private var btnHorizontal: Button? = null
    private var btnVertical: Button? = null

    @Inject
    lateinit var circleListViewModel:  CircleListViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getDeviceSizes()
        MainActivityUI().setContentView(this)


        rlMain = findViewById(R.id.rl_main)
        btnShape = findViewById(R.id.btn_shape)
        btnHorizontal = findViewById(R.id.btn_horizontal)
        btnVertical = findViewById(R.id.btn_vertical)

        btnShape!!.setOnClickListener(this)
        btnHorizontal!!.setOnClickListener(this)
        btnVertical!!.setOnClickListener(this)


        rlMain = findViewById(R.id.rl_main)


        circleListViewModel = ViewModelProviders.of(this).get(CircleListViewModel::class.java!!)

    }



    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btn_shape -> addShape()
            R.id.btn_horizontal -> {
                style = 'h'
                putCirclesInHorizontal()
            }
            R.id.btn_vertical -> {
                style = 'v'
                putCirclesInVertical()
            }
        }
    }



    fun addShape() {
        if (counter != totalCount) {
            when (style) {
                'h' -> if ((counter + 1) % countInRow != 0) {
                    val rectAngle = CircleShape(this@MainActivity, x, y)
                    val circle = MyCircle(x, y)
                    circleListViewModel!!.addCircle(circle)
                    rlMain!!.addView(rectAngle)
                    counter++
                    x += 210
                } else {
                    val rectAngle = CircleShape(this@MainActivity, x, y)
                    val circle = MyCircle(x, y)
                    circleListViewModel!!.addCircle(circle)
                    rlMain!!.addView(rectAngle)
                    counter++
                    x = 120
                    y += 210
                }
                'v' -> if ((counter + 1) % countInColumn != 0) {
                    val rectAngle = CircleShape(this@MainActivity, x, y)
                    val circle = MyCircle(x, y)
                    circleListViewModel!!.addCircle(circle)
                    rlMain!!.addView(rectAngle)
                    counter++
                    y += 210
                } else {
                    val rectAngle = CircleShape(this@MainActivity, x, y)
                    val circle = MyCircle(x, y)
                    circleListViewModel!!.addCircle(circle)
                    rlMain!!.addView(rectAngle)
                    counter++
                    y = 120
                    x += 210
                }
            }
        } else {
            Snackbar.make(rlMain!!, "You can't add new Shape", Snackbar.LENGTH_SHORT).show()
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        circleListViewModel!!.removeTable()
    }

    public override fun onStop() {
        super.onStop()
        circleListViewModel!!.removeTable()
    }


    private fun putCircles(list: List<MyCircle>) {

        rlMain!!.removeAllViews()
        for (i in list.indices) {
            val rectAngle = CircleShape(this@MainActivity, list[i].x, list[i].y)
            rlMain!!.addView(rectAngle)

        }

    }

     fun putCirclesInHorizontal() {

        circleListViewModel!!.allCircles.observe(this,
            Observer<List<MyCircle>> { circles ->
                //add notes to adapter
                x = 120
                y = 120
                for (i in circles!!.indices) {
                    if (i == 0 || i % countInRow != 0) {
                        val circle = MyCircle(x, y)
                        circleListViewModel!!.updateCircle(circle)
                        circles.get(i).x = circle.x
                        circles.get(i).y = circle.y
                        x += 210
                    } else {
                        x = 120
                        y += 210
                        val circle = MyCircle(x, y)
                        circleListViewModel!!.updateCircle(circle)
                        circles.get(i).x = circle.x
                        circles.get(i).y = circle.y
                        x += 210

                    }
                }
                putCircles(circles as List<MyCircle>)
            })


    }


     fun putCirclesInVertical() {

        circleListViewModel!!.allCircles.observe(this,
            Observer<List<MyCircle>> { circles ->
                x = 120
                y = 120
                for (i in circles!!.indices) {
                    if (i == 0 || i % countInColumn != 0) {
                        val circle = MyCircle(x, y)
                        circleListViewModel!!.updateCircle(circle)
                        circles.get(i).x = circle.x
                        circles.get(i).y = circle.y
                        y += 210
                    } else {
                        y = 120
                        x += 210
                        val circle = MyCircle(x, y)
                        circleListViewModel!!.updateCircle(circle)
                        circles.get(i).x = circle.x
                        circles.get(i).y = circle.y
                        y += 210

                    }
                }
                putCircles(circles as List<MyCircle>)
            })
    }

     fun getDeviceSizes() : Unit
    {
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        width = size.x
        height = size.y

        countInRow = (width - 10) / 210
        countInColumn = (height - bottomLayoutHeight - 20) / 210
        totalCount = countInColumn * countInRow
    }
}