package com.knock.shapeorder.dagger.components

import android.app.Application
import com.knock.shapeorder.dagger.modules.CircleListViewModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [CircleListViewModule::class])
interface MainActivityComponent {
    fun inject( app: Application)
}

