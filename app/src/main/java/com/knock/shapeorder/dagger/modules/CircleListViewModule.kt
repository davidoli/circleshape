package com.knock.shapeorder.dagger.modules

import android.app.Application
import com.knock.shapeorder.MyApplication
import com.knock.shapeorder.MyApplication.Companion.app
import com.knock.shapeorder.database.CircleDatabase
import com.knock.shapeorder.viewmodels.CircleListViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CircleListViewModule(app: Application) {
    @Provides
    @Singleton
    fun provideApp(app: Application) = CircleListViewModel(app)
}
