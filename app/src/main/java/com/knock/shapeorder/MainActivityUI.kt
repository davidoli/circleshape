package com.knock.shapeorder

import android.graphics.Color
import android.graphics.Point
import android.view.Gravity
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.knock.shapeorder.database.MyCircle
import com.knock.shapeorder.utilities.CircleShape
import com.knock.shapeorder.viewmodels.CircleListViewModel
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import javax.inject.Inject

class MainActivityUI : AnkoComponent<MainActivity> {




    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        relativeLayout()
        {
            relativeLayout()
            {
                id = R.id.rl_main
            }.lparams()
            {

            }

            linearLayout()
            {

                weightSum = 3F
                orientation = LinearLayout.HORIZONTAL
                gravity = Gravity.BOTTOM


                button() {
                    textColor = Color.BLACK
                    textSize = 14F
                    gravity = Gravity.CENTER
                    text = "Shape"
                    id = R.id.btn_shape

                }.lparams(width = 0, height = matchParent, weight = 1F)

                button() {
                    textColor = Color.BLACK
                    textSize = 14F
                    gravity = Gravity.CENTER
                    text = "Horizontal"
                    id = R.id.btn_horizontal

                }.lparams(width = 0, height = matchParent, weight = 1F)


                button() {
                    textColor = Color.BLACK
                    textSize = 14F
                    gravity = Gravity.CENTER
                    text = "Vertical"
                    id = R.id.btn_vertical
                }.lparams(width = 0, height = matchParent, weight = 1F)

            }.lparams(width = matchParent, height = 120)
            {
                bottomMargin = dip(10)
                alignParentBottom()
            }
        }

    }


}