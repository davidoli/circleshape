package com.knock.shapeorder.utilities

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class CircleShape : View {

    var myPaint: Paint ?= null
    var myCanvas: Canvas ?= null
    var x: Int = 0
    var y: Int = 0

    constructor(context: Context, x1: Int, y1: Int) : super(context) {
        x = x1
        y = y1
        initPaints()
    }

    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr) {
        initPaints()

    }

    internal fun initPaints() {
        myPaint = Paint()
        myPaint!!.style = Paint.Style.FILL
        myPaint!!.strokeWidth = 10f
        myPaint!!.color = Color.parseColor("#202020")


    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        myCanvas = canvas
        myFun(x, y)
    }


    fun myFun(x: Int, y: Int) {

        myPaint?.let { myCanvas?.drawCircle(x.toFloat(), y.toFloat(), 80f, it) }

    }


}