package com.knock.shapeorder.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

// Room database class
@Database(entities = [MyCircle::class], version = 1, exportSchema = false)
abstract class CircleDatabase : RoomDatabase() {

    //define note dao ( data access object )
    abstract fun circleDao(): CircleDao

    companion object {
        //define static instance
        private var mInstance: CircleDatabase? = null

        //method to get room database
        fun getDatabase(context: Context): CircleDatabase {

            if (mInstance == null)
                mInstance = Room.databaseBuilder(
                    context.applicationContext,
                    CircleDatabase::class.java, "circles_db"
                )
                    .build()

            return mInstance as CircleDatabase
        }

        //method to remove instance
        fun closeDatabase() {
            mInstance = null
        }
    }


}