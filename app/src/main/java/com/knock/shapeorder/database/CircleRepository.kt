package com.knock.shapeorder.database

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import javax.inject.Inject

//Circle repository
class CircleRepository(application: Application) {
    //Live Data of List of all circles
    //method to get all circles
    val allNotes: LiveData<List<MyCircle>>
    //Define Circle Dao
    internal var mCircleDao: CircleDao





    init {

        val noteDatabase = CircleDatabase.getDatabase(application)
        //init Circle Dao
        mCircleDao = noteDatabase.circleDao()
        //get all Circles
        allNotes = mCircleDao.allCircles

    }


    fun addCircle(circle: MyCircle): LiveData<List<MyCircle>> {
        AddCircle().execute(circle)
        return allNotes
    }


    fun updateCircle(circle: MyCircle) {
        updateCircle2().execute(circle)
    }


//    //Async task to update circle
    inner class updateCircle2 : AsyncTask<MyCircle, Void, Void>() {
        override fun doInBackground(vararg circles: MyCircle): Void? {
            mCircleDao.updateCircle(circles[0])
            return null
        }
    }

    //Async task to add circle
    inner class AddCircle : AsyncTask<MyCircle, Void, Void>() {
        override fun doInBackground(vararg circles: MyCircle): Void? {
            mCircleDao.insertCircle(circles[0])
            return null
        }
    }

    inner class removeTable2 : AsyncTask<MyCircle, Void, Void>() {
        override fun doInBackground(vararg circles: MyCircle): Void? {
            mCircleDao.nukeTable()
            return null
        }
    }


     fun removeTable() {
        removeTable2().execute()
    }
}