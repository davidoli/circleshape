package com.knock.shapeorder.database

import androidx.lifecycle.LiveData
import androidx.room.*

import androidx.room.OnConflictStrategy.REPLACE

@Dao
open interface CircleDao {
    // Dao method to get all circles
    @get:Query("SELECT * FROM MyCircle")
    val allCircles: LiveData<List<MyCircle>>

    // Dao method to insert circle
    @Insert(onConflict = REPLACE)
    fun insertCircle(circle: MyCircle)


    // Dao method to delete circle
    @Delete
    fun deleteCircle(circle: MyCircle)

    // Dao method to delete circle
    @Update
    fun updateCircle(circle: MyCircle)

    @Query("DELETE FROM MyCircle")
    fun nukeTable()

}