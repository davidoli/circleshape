package com.knock.shapeorder.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MyCircle constructor(var x1: Int,var y1:Int) {
    // room database entity primary key
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    var x: Int = x1
    var y: Int = y1


}