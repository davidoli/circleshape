package com.knock.shapeorder

import android.app.Application
import android.content.Context
import com.knock.shapeorder.dagger.components.DaggerMainActivityComponent

class MyApplication : Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        //        MultiDex.install(this);
    }


    override fun onCreate() {
        super.onCreate()
        app = this
        context = applicationContext
        DaggerMainActivityComponent.create().inject(this)

    }

    companion object {


        var app: Application? = null
            private set

        var context: Context? = null
            private set
    }
}
