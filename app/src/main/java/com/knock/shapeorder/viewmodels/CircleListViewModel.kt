package com.knock.shapeorder.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.knock.shapeorder.database.CircleRepository
import com.knock.shapeorder.database.MyCircle

class CircleListViewModel(application: Application) : AndroidViewModel(application) {
    val allCircles: LiveData<List<MyCircle>>
     var mCircleRepository: CircleRepository

    init {
        mCircleRepository = CircleRepository(application)
        allCircles = mCircleRepository.allNotes
    }

    //    public void addCircle(MyCircle circle) {
    //        mCircleRepository.addCircle(circle);
    //    }

    fun addCircle(circle: MyCircle): LiveData<List<MyCircle>> {
        mCircleRepository.addCircle(circle)
        return allCircles
    }

    fun updateCircle(circle: MyCircle) {
        mCircleRepository.updateCircle(circle)
    }

    fun removeTable() {
        mCircleRepository.removeTable()
    }
}